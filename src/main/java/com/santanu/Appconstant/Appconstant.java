package com.santanu.Appconstant;

public interface Appconstant {
	public static String FORWARD_SLASH="/";
	public static String SAVE_REGISTER_DETAILS="/saveRegisterDetails";
	public static String REGISTER_INFO="Register_details";
	public static String GET_ALL_REGISTER_DETAILS="/getRegisterDetails";
	public static String SAVE_REGISTER="/saveRegisterDetails";
	public static String DELETE_REGISTER="/deleteRegisterDetails";

}
