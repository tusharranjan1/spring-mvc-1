package com.santanu.repositiry;


import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


import com.santanu.entity.Register;
//@Component
@Repository
public class RegisterRepositiry {
	@Autowired
	private SessionFactory sessionFactory;
	 public void save(Register register) {
		 Session session=sessionFactory.openSession();
		 try {
			 Transaction transaction =session.beginTransaction();
			 session.merge(register);
			 transaction.commit();
		 }
		 catch(Exception e) {
		 } finally {
				 session.close();
			 }
		 }
	   public List<Register> findAll() {
		   Session session=sessionFactory.openSession();
		   try {
			   String hql="from Register";
			   Query query=session.createQuery(hql);
			   return query.list();
		   }
		   catch(Exception e) {
		   }finally {
				   session.close();
			   }
		   return Collections.EMPTY_LIST;
		   }
	   
    public void delete() {
          Session session=sessionFactory.openSession();
         Transaction trans= session.beginTransaction();
       
    	   try {
    	String hql="delete from Register r where r.Name= :santanu";	
    	Query query=session.createQuery(hql);
    	trans.commit();
    	   }
    	   catch(Exception e) {
    		   
    	   }finally {
    		   session.close();
    	   }
       
    }
	   }
		 

