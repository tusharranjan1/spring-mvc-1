package com.santanu.javaconfiguration;


import java.util.Properties;

import javax.sql.DataSource;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("com.santanu")
public class AppConfiguration {
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}
	  @Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean sessionFactory=new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		sessionFactory.setPackagesToScan("com.santanu.entity");
		sessionFactory.setHibernateProperties(null);
		Properties properties=new Properties();
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hbm2ddl.auto", "update");
		sessionFactory.setHibernateProperties(properties);
		return sessionFactory;
	}
	
	  @Bean
       public DataSource getDataSource() {
    	   DriverManagerDataSource dataSource= new DriverManagerDataSource();
    	   dataSource.setUrl("jdbc:mysql://localhost:3306/rrr");
    	   dataSource.setUsername("root");
    	   dataSource.setPassword("SKsahoo@123");
    	   dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    	   return dataSource;
       }
}
