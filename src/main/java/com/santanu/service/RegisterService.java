package com.santanu.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.santanu.dtoclass.RegistationFormDto;
import com.santanu.entity.Register;
import com.santanu.repositiry.RegisterRepositiry;

//@Component
@Service
public class RegisterService {
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private RegisterRepositiry registerRepository;

	public void processRegisterDetails(RegistationFormDto registationform) {
		Register register = modelMapper.map(registationform, Register.class);
		registerRepository.save(register);

	}
	public List<Register> findAll(){
		return registerRepository.findAll();
	}
   public void Deleted() {
	   registerRepository.delete();
   }
}
