package com.santanu.dtoclass;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

public class RegistationFormDto implements Serializable {
	private String Name;
	private String Role;
	private String Email;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	@Override
	public String toString() {
		return "RegistationFormDto [Name=" + Name + ", Role=" + Role + ", Email=" + Email + "]";
	}
	
	
	

}
