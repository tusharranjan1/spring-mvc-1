package com.santanu.springwebclass;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.santanu.Appconstant.Appconstant;
import com.santanu.dtoclass.RegistationFormDto;
import com.santanu.service.RegisterService;

@Component
//@Controller
@RequestMapping(value = Appconstant.FORWARD_SLASH)
public class RegisterController {
	@Autowired
	private RegisterService registerService;

	public RegisterController() {
		System.out.println(this.getClass().getSimpleName() + "creating object of RegisterController");
	}

	@RequestMapping(value = Appconstant.SAVE_REGISTER_DETAILS)
	public ModelAndView saveRegisterdetails(RegistationFormDto registationform) {
		System.out.println(registationform);
		registerService.processRegisterDetails(registationform);
		return new ModelAndView("display.jsp");
	}

}
