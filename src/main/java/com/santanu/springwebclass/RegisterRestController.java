package com.santanu.springwebclass;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.santanu.Appconstant.Appconstant;
import com.santanu.dtoclass.RegistationFormDto;
import com.santanu.entity.Register;
import com.santanu.service.RegisterService;
//@Controller
//@ResponseBody
@RestController// to convert java object to json object also create object for class
@RequestMapping(value=Appconstant.FORWARD_SLASH)
public class RegisterRestController {
	@Autowired
	  private RegisterService registerService;
	@GetMapping(value=Appconstant.GET_ALL_REGISTER_DETAILS)
	 public @ResponseBody List<Register> getRegisterDetails() {
		 List<Register> registerList=registerService.findAll();
		 return registerList;
		 
	 }
	@PostMapping(value=Appconstant.SAVE_REGISTER)
	public void saveRegisterRestDetails(@RequestBody RegistationFormDto registationformDto ) {
		registerService.processRegisterDetails(registationformDto);	
		
	}
	@DeleteMapping(value=Appconstant.DELETE_REGISTER)
	public @ResponseBody void deleteRegisterDetails() {
		registerService.Deleted();

}
}