package com.santanu.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.GenericGenerators;

import com.santanu.Appconstant.Appconstant;
@Entity
@Table(name=Appconstant.REGISTER_INFO)
public class Register implements Serializable {
	@Id
	@GenericGenerator(name="auto",strategy="increment")
	@GeneratedValue(generator="auto")
	@Column(name="Id")
	private Long Id;
	@Column(name="Name")
	private String Name;
	@Column(name="Role")
	private String Role;
	@Column(name="Email")
	private String Email;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	@Override
	public String toString() {
		return "Register [Id=" + Id + ", Name=" + Name + ", Role=" + Role + ", Email=" + Email + "]";
	}
	

}
